# Woofing chez Léon, Juillet / Août 2020

Je suis Marius, 32 ans, développeur web en télétravail.

Je souhaite créer un lieu d'accueil et d'hébergement insolite, sous tente, avec table d'hôte fournit par le potager et les producteurs locaux.

J'ai acheté pour ça une grange pour la transformer en maison et lieu de vie, avec un terrain de 2500m2

Il y a à peu près tout à faire,  mais cet année je souhaite avancer sur le jardin, et la mise au propre de la grange avant d'attaquer le gros œuvre.

C'est pour ça que je cherche des volontaires pour du woofing cet été 🙂

### Au programme :

    Aménagement du jardin et entretien/amélioration du potager en place

    Amélioration du l'installation électrique solaire en place

    Démontage et déblayage d'un mur de la grange 

    Creuser la phytoepuration 

    Construction d'une cabane de toilettes sèches 

    Etc...


### Ce que je propose : 

    Logé/ nourri / blanchi

    Hébergement sous tente (quechua, ou tente mongole si ça passe dans le budget de l'été)

    Je bosse en télétravail la journée,  donc l'idée serait se bosser ensemble ~1h30 le matin, et 2-3h le soir, tu as la journée libre ensuite :)

Je cherche quelqu'un d'autonome, débutant ou non, si possible motorisé, ou avec le permis AM, je peux prêter mon scooter !

Je suis entre Périgueux et Angouleme, accès facile en TGV + bus, dans le Périgord vert, et je commence à connaitre pas mal la région,  si tu as besoin de conseils en balade / visite pour occuper tes journées !

Attention : il y a une douche mais pas d'eau chaude pour l'instant 😇

Quelques photos :  [gallerie framapic](https://framapic.org/gallery#GJI2fnTWWcGK/gA6k0UArGCnP.jpg,g1CKRg5XxVj5/I7wpj5UYthHP.jpg,eAiVCPAoZW23/cIz6jRq5FVQJ.jpg,nqE6DAvAERXV/FhSwALmKnzFP.jpg,JwmzWytblesy/kwEB1NHZBYNZ.jpg,fSz0eEYhLH5t/xEQo7IlqYysV.jpg,2cq8V3X7rYR4/mConSx14WXPm.jpg,8ahiedNPI9ug/VAPz1tMA4LTF.jpg,rlafAFsvE2PZ/kkTnDdLB6WW4.jpg,yCrnvqYAkeqr/hWIu13XPwhI9.jpg)